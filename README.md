## Shopping List Assignment

**Test assignment:**

 Shopping list application. The application should make everyday
 shopping easier by enabling user to list down items. While shopping,
 the application helps to track items which are already collected.
 
 Non-functional requirements:
 
 1) Platform: iOS (version is not important)
 
 Functional requirements:
  1. Add items into list (item has only one value: String name)
  2. Delete items from list
  3. Check/uncheck items within list
  4. Itemlist must be persistent (when the application dies for some
  reason, it should be able to present previous list again)
 
 In order to make application more usable, any other functionality may<br/>
 be added (e.g. several itemlist support; item name autocompletion<br/>
 based on previous item names; etc)
 
---------------------------------------------------------
Implemented Features
* Multiple Lists
* Delete items from List with long press
* Check/Uncheck items within list
* Persistence of items, history, products
* Autocomplete

## Mock Diagram

![WP_002039.jpg](https://bitbucket.org/repo/MR49X8/images/3213905167-WP_002039.jpg)