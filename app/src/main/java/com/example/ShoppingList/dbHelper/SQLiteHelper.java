package com.example.ShoppingList.dbHelper;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.ShoppingList.constant.DBConstants;

public class SQLiteHelper extends SQLiteOpenHelper {

	Resources resources;


	public SQLiteHelper(final Context context) {
		super(context, DBConstants.DB_NAME, null, 1);
		resources = context.getResources();

	}

	@Override
	public void onCreate(final SQLiteDatabase db) {

        // Table: History
        db.execSQL("CREATE TABLE " + DBConstants.TAB_HISTORY_NAME + " ("
                + DBConstants.COL_HISTORY_ID + " INTEGER PRIMARY KEY NOT NULL, "
                + DBConstants.COL_HISTORY_NAME + " VARCHAR(75) NOT NULL);");

		// Table: Product
		db.execSQL("CREATE TABLE " + DBConstants.TAB_PRODUCT_NAME + " ("
				+ DBConstants.COL_PRODUCT_ID + " INTEGER PRIMARY KEY NOT NULL, "
                + DBConstants.COL_PRODUCT_NAME + " VARCHAR(100) NOT NULL, "
                + DBConstants.COL_PRODUCT_LST_ID + " INTEGER NOT NULL, "
                + DBConstants.COL_PRODUCT_CHECKED + " SMALLINT DEFAULT 0, FOREIGN KEY ("
				+ DBConstants.COL_PRODUCT_LST_ID + ") REFERENCES " + DBConstants.TAB_LIST_NAME
				+ "(" + DBConstants.COL_LIST_ID + "));");

		/// Table: List
        db.execSQL("CREATE TABLE " + DBConstants.TAB_LIST_NAME + " ("
                + DBConstants.COL_LIST_ID + " INTEGER PRIMARY KEY NOT NULL, "
                + DBConstants.COL_LIST_NAME + " VARCHAR(75) NOT NULL);");
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {

	}
}
