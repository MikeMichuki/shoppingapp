package com.example.ShoppingList.dbHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ShoppingList.bean.History;
import com.example.ShoppingList.bean.Lists;
import com.example.ShoppingList.bean.Product;
import com.example.ShoppingList.constant.DBConstants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class ListItemsDataSource{
    private SQLiteDatabase database;
    private final SQLiteHelper dbHelper;

    /**
     * Constructor
     *
     * @param context
     */
    public ListItemsDataSource(Context context) {
		this.dbHelper = new SQLiteHelper(context);
    }

    /**
     * opens the database
     */
    public void open() {
        try {
            this.database = this.dbHelper.getWritableDatabase();
        } catch (final SQLException se) {
            se.printStackTrace();
        }
    }

    /**
     * closes the db-connection
     */
    public void close() {
        // this.isDbLockedByThread();

        if ((this.database != null) && !this.database.isDbLockedByOtherThreads()
                && this.database.isOpen()) {
            if (this.dbHelper != null) {
                this.dbHelper.close();
            }

        }
    }
    /**
     * Saves a List with the given name
     *
     * @param name
     */
    public void saveList(String name) {
        this.isDbLockedByThread();

        final String sqlQuery = "INSERT INTO " + DBConstants.TAB_LIST_NAME + " ("
                + DBConstants.COL_LIST_NAME + ") VALUES ('" + name.trim() + "')";

        this.database.execSQL(sqlQuery);
    }

    /**
     * Saves a product with the given name, checked and store id
     *
     * @param name
     * @param checked
     */
    public void saveProduct(String name, final short checked, int listId) {
        this.isDbLockedByThread();
        final String sqlQuery = "INSERT INTO " + DBConstants.TAB_PRODUCT_NAME
                + " (" + DBConstants.COL_PRODUCT_NAME + ", "
                + DBConstants.COL_PRODUCT_LST_ID + ", "
                + DBConstants.COL_PRODUCT_CHECKED  + ") VALUES ('"
                + name.trim() + "', " + listId + ", " + checked + ")";
        this.database.execSQL(sqlQuery);
        saveHistory(name);
    }
    /**
     * deletes an given product by productId
     *
     * @param productID
     */
    public void deleteProduct(final int productID) {
        this.isDbLockedByThread();

        final String sqlQuery = "DELETE FROM " + DBConstants.TAB_PRODUCT_NAME + " WHERE "
                + DBConstants.COL_PRODUCT_ID + " = " + productID;

        this.database.execSQL(sqlQuery);
    }

    /**
     * deletes an given list by listId
     *
     * @param listId
     */
    public void deleteAllProductsOfList(final int listId) {
        this.isDbLockedByThread();

        final String sqlQuery = "DELETE FROM " + DBConstants.TAB_PRODUCT_NAME + " WHERE "
                + DBConstants.COL_PRODUCT_LST_ID + " = " + listId;

        this.database.execSQL(sqlQuery);
    }

    /**
     * deletes an given list by listId
     *
     * @param listId
     */
    public void deleteList(final int listId) {
        this.isDbLockedByThread();

        deleteAllProductsOfList(listId);
        final String sqlQuery = "DELETE FROM " + DBConstants.TAB_LIST_NAME + " WHERE "
                + DBConstants.COL_LIST_ID + " = " + listId;

        this.database.execSQL(sqlQuery);
    }

    /**
     * marks the product as checked
     *
     * @param product
     */
    public void markProductAsChecked(final Product product) {
        this.isDbLockedByThread();

        final String sqlQuery = "UPDATE " + DBConstants.TAB_PRODUCT_NAME
                + " SET " + DBConstants.COL_PRODUCT_CHECKED + " = "
                + product.isChecked() + " WHERE " + DBConstants.TAB_PRODUCT_NAME
                + "." + DBConstants.COL_PRODUCT_ID + " = "
                + product.getId();

        this.database.execSQL(sqlQuery);
    }
    /**
     * updates a product with new product
     *
     * @param product
     *
     */
    public void updateProduct(final Product product) {
        this.isDbLockedByThread();

        final String sqlQuery = "UPDATE " + DBConstants.TAB_PRODUCT_NAME + " SET "
                + DBConstants.COL_PRODUCT_NAME + " = '" + product.getName().trim() + "' , "
                + DBConstants.COL_PRODUCT_CHECKED + " = " + product.isChecked() +  " WHERE "
                + DBConstants.TAB_PRODUCT_NAME + "." + DBConstants.COL_PRODUCT_ID + " = "
                + product.getId();

        this.database.execSQL(sqlQuery);
    }

    /**
     * gets all products from the DB (Table: Product)
     *
     * @return List<String> productNames
     */
    public List<Lists> getAllLists() {
        this.isDbLockedByThread();

        final String sqlQuery = "SELECT "+ DBConstants.COL_LIST_ID + ", "
                + DBConstants.COL_LIST_NAME + " FROM " + DBConstants.TAB_LIST_NAME + " ORDER BY "
                + DBConstants.COL_LIST_NAME;

        final Cursor cursor = this.database.rawQuery(sqlQuery, null);

        final List<Lists> lists = new LinkedList<Lists>();

        while (cursor.moveToNext()) {
            final Lists list = new Lists();
            list.setId(cursor.getInt(cursor.getColumnIndex(DBConstants.COL_LIST_ID)));
            list.setName(cursor.getString(cursor.getColumnIndex(DBConstants.COL_LIST_NAME)));
            lists.add(list);
        }
        cursor.close();

        return lists;
    }

    /**
     * gets all product names for Auto-Complete from the DB (Table: History)
     *
     * @return List<String> historyNames
     */
    public List<String> getAutoCompleteHistoryNames() {
        this.isDbLockedByThread();

        final String sqlQuery = "SELECT DISTINCT " + DBConstants.COL_HISTORY_NAME + " FROM "
                + DBConstants.TAB_HISTORY_NAME + " ORDER BY " + DBConstants.COL_HISTORY_NAME;

        final Cursor cursor = this.database.rawQuery(sqlQuery, null);

        final List<String> historyNames = new ArrayList<String>();

        while (cursor.moveToNext()) {
            String historyName = cursor.getString(cursor
                    .getColumnIndex(DBConstants.COL_HISTORY_NAME));
            historyNames.add(historyName);
        }
        cursor.close();

        return historyNames;
    }

    /**
     * gets all products from the DB (Table: Product) filtered by listId
     *
     * @return List<String> productNames
     */
    public List<Product> getAllProductsByListId(int listId) {
        this.isDbLockedByThread();

        final String sqlQuery = "SELECT "
                + DBConstants.COL_PRODUCT_ID + ", " + DBConstants.COL_PRODUCT_NAME + ", "
                + DBConstants.COL_PRODUCT_LST_ID + ", " + DBConstants.COL_PRODUCT_CHECKED + " FROM "
                + DBConstants.TAB_PRODUCT_NAME + " WHERE "
                + DBConstants.TAB_PRODUCT_NAME + "." + DBConstants.COL_PRODUCT_LST_ID + " = " + listId
                + " ORDER BY " + DBConstants.COL_PRODUCT_NAME;
        Log.d("SQL", "Get All Products by List QUERY:" + sqlQuery);

        final Cursor cursor = this.database.rawQuery(sqlQuery, null);

        final List<Product> products = new ArrayList<Product>();

        while (cursor.moveToNext()) {
            final Product product = new Product();
            product.setId(cursor.getInt(cursor.getColumnIndex(DBConstants.COL_PRODUCT_ID)));
            product.setName(cursor.getString(cursor.getColumnIndex(DBConstants.COL_PRODUCT_NAME)));
            product.setChecked(cursor.getShort(cursor.getColumnIndex(DBConstants.COL_PRODUCT_CHECKED)));
            products.add(product);
        }
        cursor.close();

        return products;
    }

    /**
     * gets all History names for Auto-Complete from the DB (Table: History)
     *
     * @return List<String> HistoryNames
     */
    public List<History> getAllHistory() {
        this.isDbLockedByThread();
        final String sqlQuery = "SELECT "
                + DBConstants.COL_HISTORY_ID + ", "
                + DBConstants.COL_HISTORY_NAME + " FROM "
                + DBConstants.TAB_HISTORY_NAME + " ORDER BY " + DBConstants.COL_HISTORY_NAME;
        final Cursor cursor = this.database.rawQuery(sqlQuery, null);

        Log.d("SQL", "Get All History QUERY:" + sqlQuery);

        final List<History> historyNames = new ArrayList<History>();

        while (cursor.moveToNext()) {
            final History history = new History();
            history.setId(cursor.getInt(cursor.getColumnIndex(DBConstants.COL_HISTORY_ID)));
            history.setName(cursor.getString(cursor.getColumnIndex(DBConstants.COL_HISTORY_NAME)));
            historyNames.add(history);
        }
        cursor.close();

        return historyNames;
    }
    /**
     * Saves a History with the given name and unitId
     *
     * @param name
     */
    public void saveHistory(String name) {
        this.isDbLockedByThread();
        final String sqlQuery = "INSERT INTO " + DBConstants.TAB_HISTORY_NAME
                + " (" + DBConstants.COL_HISTORY_NAME + ") VALUES ('"+ name.trim()+"')";
        this.database.execSQL(sqlQuery);
    }
    /**
     * deletes the whole history
     *
     */
    public void deleteHistory() {
        this.isDbLockedByThread();
        final String sqlDeleteHistory = "DELETE FROM " + DBConstants.TAB_HISTORY_NAME;
        this.database.execSQL(sqlDeleteHistory);
    }

    /**
     * deletes the all products
     *
     */
    public void deleteProducts() {
        this.isDbLockedByThread();
        final String sqlDeleteProducts = "DELETE FROM " + DBConstants.TAB_PRODUCT_NAME;
        this.database.execSQL(sqlDeleteProducts);
    }

    /**
     *
     * Checks whether the DB is locked by a thread (current or other). To avoid
     * deadlock-infinite-loop there's a counter.
     *
     */
    private void isDbLockedByThread() {
        int counter = 0;
        while (((this.database != null)
                && (this.database.isDbLockedByCurrentThread() || this.database
                .isDbLockedByOtherThreads()) && (counter < 1000))) {
            counter++;
        }
    }
}
