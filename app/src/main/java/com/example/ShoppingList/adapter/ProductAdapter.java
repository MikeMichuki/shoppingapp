package com.example.ShoppingList.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.ShoppingList.R;
import com.example.ShoppingList.bean.Product;
import com.example.ShoppingList.constant.GlobalValues;

import java.util.List;

/*
* Product Adapter
* Description: This page serves the Products Page
* It populates the TextView and the Checkbox for each product
* when the user taps to check.
* Created by Michael on 15/04/2015.
* */
public class ProductAdapter extends ArrayAdapter<Product> {

    private final Context context;

    private final List<Product> values;

    public ProductAdapter(final Context context,
                                             final List<Product> values) {
        super(context, R.layout.product_item, values);
        this.context = context;
        this.values = values;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {

        final LayoutInflater inflater = (LayoutInflater) this.context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.product_item, parent,
                false);

        final TextView textView = (TextView) rowView.findViewById(R.id.product_textView);

        final CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.product_checked);

        final Product product = this.values
                .get(position);

        textView.setText(product.getName().toString());

        if (product.isChecked() == GlobalValues.YES) {
            checkBox.setChecked(true);

        } else if (product.isChecked() == GlobalValues.NO) {
            checkBox.setChecked(false);
        }

        return rowView;
    }
}
