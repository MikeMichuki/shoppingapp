package com.example.ShoppingList.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ShoppingList.R;
import com.example.ShoppingList.bean.History;

import java.util.List;

/*
* History Adapter
* Description: This Adapter serves the TextView
* in the History listItemView page
* Created by Michael on 15/04/2015.
* */
public class HistoryAdapter extends ArrayAdapter<History> {

	@SuppressWarnings("unused")
	private final Context context;

	private final List<History> values;

	public HistoryAdapter(final Context context, final List<History> values) {
		super(context, R.layout.history_item, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final TextView textView = (TextView)
                super.getView(position, convertView, parent);

		final History historyToBeShown = this.values.get(position);

		String textToShow = historyToBeShown.getName();

		textView.setText(textToShow);

		return textView;
	}

}
