package com.example.ShoppingList.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.ShoppingList.R;
import com.example.ShoppingList.bean.Lists;

import java.util.List;

/*
* Created by Michael on 15/04/2015.
* List Adapter
* Description: This Adapter serves the TextView
* in the Main Page, the page with the Lists
*
* */
public class ListsAdapter extends ArrayAdapter<Lists> {

	private final Context context;
	private final List<Lists> values;

	public ListsAdapter(final Context context, final List<Lists> values) {
		super(context, R.layout.list_item, values);
		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        final TextView textView = (TextView)super.getView(position, convertView, parent);

        final Lists itemsToBeShown = this.values.get(position);

		String textToShow = itemsToBeShown.getName();

		textView.setText(textToShow);

		return textView;
	}

}
