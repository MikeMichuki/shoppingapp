package com.example.ShoppingList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.ShoppingList.adapter.HistoryAdapter;
import com.example.ShoppingList.bean.History;
import com.example.ShoppingList.dbHelper.ListItemsDataSource;

import java.util.List;

public class HistoryActivity extends Activity {

    private HistoryAdapter historyAdapter;
    private List<History> historyList;
    private ListView listViewHistory;

    private ListItemsDataSource dataSource;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        this.context = this;
        this.dataSource = new ListItemsDataSource(this.context);
        this.dataSource.open();

        displayListItems();
    }
    private void displayListItems(){
        this.listViewHistory = (ListView) this.findViewById(R.id.history_listView);
        this.historyList = this.dataSource.getAllHistory();
        this.historyAdapter = new HistoryAdapter(this.context,this.historyList);
        this.listViewHistory.setAdapter(this.historyAdapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;

            // AddProductbutton - Actionbar
            case R.id.action_Clear_History:
                final AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
                alertBox.setMessage("You are About to Empty your History! Are you sure?");
                alertBox.setPositiveButton("yes", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        HistoryActivity.this.dataSource.deleteHistory();
                        HistoryActivity.this.historyAdapter.clear();
                        displayListItems();
                    }
                });

                alertBox.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        // do nothing here
                    }
                });

                alertBox.show();

                break;
            default:
                break;
        }

        return false;
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
