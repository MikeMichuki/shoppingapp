package com.example.ShoppingList.constant;

public class DBConstants {
    public static String DB_NAME = "shoppinglist.db";

    public static int DB_VERSION = 1;

	public static String TAB_PRODUCT_NAME = "PRODUCT";

    public static String COL_PRODUCT_ID = "PRO_ID";

    public static String COL_PRODUCT_NAME = "PRO_NAME";

    public static String COL_PRODUCT_LST_ID = "PRO_LST_ID";

    public static String COL_PRODUCT_CHECKED = "PRO_CHECKED";

    public static String COL_IS_DEL = "IS_DEL";

    public static String TAB_HISTORY_NAME = "HISTORY";

    public static String COL_HISTORY_ID = "HIS_ID";

    public static String COL_HISTORY_NAME = "HIS_NAME";

    public static String TAB_LIST_NAME = "LIST";

    public static String COL_LIST_ID = "LST_ID";

    public static String COL_LIST_NAME = "LST_NAME";
}
