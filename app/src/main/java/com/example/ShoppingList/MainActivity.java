package com.example.ShoppingList;
/**
 * Created by Michael on 13/04/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.example.ShoppingList.adapter.ListsAdapter;
import com.example.ShoppingList.bean.Lists;
import com.example.ShoppingList.bean.Product;
import com.example.ShoppingList.constant.DBConstants;
import com.example.ShoppingList.dbHelper.ListItemsDataSource;

import java.util.List;


public class MainActivity extends Activity {
    ListView itemListView;
	private ListsAdapter listAdapter;
    private ListItemsDataSource dataSource;
    private List<Lists> lists;
    private Context context;
    EditText enterListNameText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

        this.context = this;
        this.dataSource = new ListItemsDataSource(this.context);
        this.dataSource.open();
        displayListItems();
        enterListNameHandler();
        listsItemLongClickHandler();
        listItemClickHandler();
    }

    private void listsItemLongClickHandler() {
        // handle long clicks on the list items
        this.itemListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(final AdapterView<?> arg0, final View v,
                                           final int position, final long id) {

                // show popup menu
                final PopupMenu popup = new PopupMenu(MainActivity.this.context, v);
                final MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popupmenu_list_overview, popup.getMenu());
                popup.show();

                // handle clicks on the popup-buttons
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(final MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.popupDeleteProductEntry:
                                // delete from mapping
                                final Lists listToDelete = MainActivity.this.listAdapter
                                        .getItem(position);
                                MainActivity.this.dataSource
                                        .deleteList(listToDelete.getId());
                                MainActivity.this.listAdapter
                                        .remove(listToDelete);

                                return true;
                            default:
                                return false;
                        }
                    }

                });

                return false;
            }

        });
    }


    private void listItemClickHandler() {
        this.itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(final AdapterView<?> arg0, final View v, final int position,
                                    final long id) {
                final Lists listItem = MainActivity.this.listAdapter
                        .getItem(position);
                Intent intent = new Intent(v.getContext(), ProductsActivity.class);
                int itemId = listItem.getId();
                String Name = listItem.getName();

                intent.putExtra(DBConstants.COL_LIST_ID, itemId);
                intent.putExtra(DBConstants.COL_LIST_NAME, Name);
                MainActivity.this.startActivityForResult(intent, 0);
            }
        });
    }

    private void enterListNameHandler() {
        enterListNameText  = (EditText) findViewById(R.id.item_textbox);
        enterListNameText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // If the enter key is pressed...
                if ((actionId & EditorInfo.IME_ACTION_GO) > 0 || (actionId & EditorInfo.IME_ACTION_SEND) > 0 ||
                        (event.getAction() == KeyEvent.ACTION_DOWN &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER && enterListNameText.getText().length() > 0)) {
                    enterListNameText.requestFocus();
                    String productName;
                    productName = enterListNameText
                            .getText().toString();

                    MainActivity.this.dataSource.saveList(productName);
                    displayListItems();
                    return true;
                }
                // We may also get a key up event.
                if (event.getAction() == KeyEvent.ACTION_UP &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    enterListNameText.setText("");
                    enterListNameText.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    private void displayListItems(){
        this.itemListView = (ListView) this.findViewById(R.id.main_listView);
        this.lists = this.dataSource.getAllLists();
        this.listAdapter = new ListsAdapter(this.context, this.lists);
        this.itemListView.setAdapter(this.listAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu,menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {

            // AddProductbutton - Actionbar
            case R.id.action_history:
                // switch to the addProductActivity
                final Intent intent = new Intent(this, HistoryActivity.class);
                this.startActivityForResult(intent, 0);
                break;
                // OptionsMenu - Actionbar
            case R.id.action_Clear_History:
                final AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
                alertBox.setMessage("You are About to Empty your History! Are you sure?");
                alertBox.setPositiveButton("yes", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        MainActivity.this.dataSource.deleteHistory();
                    }
                });

                alertBox.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        // do nothing here
                    }
                });
                break;

            default:
                break;
        }

        return false;
    }
}
