package com.example.ShoppingList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ShoppingList.adapter.ProductAdapter;
import com.example.ShoppingList.bean.Product;
import com.example.ShoppingList.constant.DBConstants;
import com.example.ShoppingList.constant.GlobalValues;
import com.example.ShoppingList.dbHelper.ListItemsDataSource;

import java.util.List;


public class ProductsActivity extends Activity {
    ListView productsListView;
    private ListItemsDataSource dataSource;
    private ProductAdapter productAdapter;
    private List<Product> products;
    private Context context;
    AutoCompleteTextView enterListNameText;
    private int listId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        this.dataSource = new ListItemsDataSource(this);
        this.dataSource.open();
        setContentView(R.layout.activity_products);

        displayListItems();
        enterProductNameHandler();
        productClickHandler();
        productLongClickHandler();
    }

    private void productLongClickHandler() {
        // handle long clicks on the list items
        this.productsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(final AdapterView<?> arg0, final View v,
                                           final int position, final long id) {

                // show popup menu
                final PopupMenu popup = new PopupMenu(ProductsActivity.this.context, v);
                final MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.popupmenu_product_overview, popup.getMenu());
                popup.show();

                // handle clicks on the popup-buttons
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(final MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.popupDeleteProductEntry:
                                // delete from mapping
                                final Product productToDelete = ProductsActivity.this.productAdapter
                                        .getItem(position);
                                ProductsActivity.this.dataSource
                                        .deleteProduct(productToDelete.getId());
                                ProductsActivity.this.productAdapter
                                        .remove(productToDelete);

                                return true;
                            default:
                                return false;
                        }
                    }

                });

                return false;
            }

        });
    }

    private void productClickHandler() {
        // handle "normal" clicks on the list items
        this.productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(final AdapterView<?> arg0, final View v, final int position,
                                    final long id) {

                final Product product = ProductsActivity.this.productAdapter
                        .getItem(position);

                if (product.isChecked() == GlobalValues.NO) {

                    ProductsActivity.this.products.get(
                            ProductsActivity.this.products
                                    .indexOf(product)).setChecked(GlobalValues.YES);
                    ProductsActivity.this.dataSource
                            .markProductAsChecked(product);
                } else if (product.isChecked() == GlobalValues.YES) {

                    ProductsActivity.this.products.get(
                            ProductsActivity.this.products
                                    .indexOf(product)).setChecked(GlobalValues.NO);
                    ProductsActivity.this.dataSource
                            .markProductAsChecked(product);
                }
                ProductsActivity.this.productAdapter.notifyDataSetChanged();
            }

        });
    }

    /*
    * Text watcher class
    * Combines both the edit text and monitors on TextChanged behaviour.
    * */
    public TextWatcher getTextWatcher(final int editTextId) {
        final EditText editText = (EditText) this.findViewById(editTextId);

        final TextWatcher textWatcher = new TextWatcher() {

            public void afterTextChanged(final Editable editable) {
                // no operation
            }

            public void beforeTextChanged(final CharSequence s, final int start, final int count,
                                          final int after) {
                // no operation

            }

            public void onTextChanged(final CharSequence s, final int start, final int before,
                                      final int count) {
                // if the text changed clear the error message in the edittext.
                editText.setError(null);
            }
        };
        return textWatcher;
    }

    private void enterProductNameHandler() {
        enterListNameText  = (AutoCompleteTextView) findViewById(R.id.enter_Product_edit_text);
        /**To DO: Implement Autocomplete **/
        this.enterListNameText.addTextChangedListener(getTextWatcher(R.id.enter_Product_edit_text));
        this.enterListNameText.setAdapter(new ArrayAdapter<String>(this, R.layout.product_autocomplete,
                this.dataSource.getAutoCompleteHistoryNames()));
        enterListNameText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // If the enter key is pressed...
                if ((actionId & EditorInfo.IME_ACTION_GO) > 0 || (actionId & EditorInfo.IME_ACTION_SEND) > 0 ||
                        (event.getAction() == KeyEvent.ACTION_DOWN &&
                                event.getKeyCode() == KeyEvent.KEYCODE_ENTER && enterListNameText.getText().length() > 0)) {
                    enterListNameText.requestFocus();
                    String productName;
                    productName = enterListNameText
                            .getText().toString();

                    ProductsActivity.this.dataSource.saveProduct(productName, GlobalValues.NO, listId);
                    displayListItems();
                    return true;
                }
                // We may also get a key up event.
                if (event.getAction() == KeyEvent.ACTION_UP &&
                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    enterListNameText.setText("");
                    enterListNameText.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    private void displayListItems(){
        Bundle extras = getIntent().getExtras();
        this.listId = this.getIntent().getIntExtra(DBConstants.COL_LIST_ID, -1);
        final String ListName = this.getIntent().getStringExtra(DBConstants.COL_LIST_NAME);
        this.productsListView = (ListView) this.findViewById(R.id.products_listView);
        this.products = this.dataSource.getAllProductsByListId(listId);
        this.productAdapter = new ProductAdapter(this.context, this.products);
        this.productsListView.setAdapter(this.productAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // AddProductbutton - Actionbar
            case R.id.action_history:
                // switch to the addProductActivity
                final Intent intent = new Intent(this, HistoryActivity.class);
                this.startActivityForResult(intent, 0);
                break;
            // OptionsMenu - Actionbar

            case R.id.action_Clear_Products:
                final AlertDialog.Builder clearProductsBox = new AlertDialog.Builder(this);
                clearProductsBox.setMessage("You are About to Delete All products! Are you sure?");
                clearProductsBox.setPositiveButton("yes", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        ProductsActivity.this.dataSource.deleteProducts();
                        Toast.makeText(getApplicationContext(), "All Products Cleared", Toast.LENGTH_LONG).show();
                    }
                });

                clearProductsBox.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        // do nothing here
                    }
                });
                break;
            // OptionsMenu - Actionbar
            case R.id.action_Clear_History:
                final AlertDialog.Builder clearBox = new AlertDialog.Builder(this);
                clearBox.setMessage("You are About to Empty your History! Are you sure?");
                clearBox.setPositiveButton("yes", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        ProductsActivity.this.dataSource.deleteHistory();
                    }
                });
                clearBox.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(final DialogInterface dialog, final int which) {
                        // do nothing here
                    }
                });
                break;

            default:
                break;
        }

        return false;
    }
}
