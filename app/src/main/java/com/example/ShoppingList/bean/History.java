package com.example.ShoppingList.bean;

/**
 * Created by Michael on 15/04/2015.
 */

public class History extends BaseColumns{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
