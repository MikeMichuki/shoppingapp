package com.example.ShoppingList.bean;

public abstract class BaseColumns {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
