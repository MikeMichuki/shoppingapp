package com.example.ShoppingList.bean;

/**
 * Created by Michael on 16/04/2015.
 */
public class Lists extends BaseColumns{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
