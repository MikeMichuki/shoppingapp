package com.example.ShoppingList.bean;

/**
 * Created by Michael on 15/04/2015.
 */
public class Product extends BaseColumns{
    private String name;
    private short checked;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int isChecked() {
        return checked;
    }
    public void setChecked(short checked) {
        this.checked = checked;
    }
}
